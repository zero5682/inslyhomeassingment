<?php

namespace App\Calculator;

use App\Calculator\Model\Installment;

class Installments
{
    public $basePrice;
    public $commission;
    public $tax;
    public $policy;
    public $grand_total;

    private $_installments = [];

    public function __construct($basePrice, $policy, $commission, $tax, $installment)
    {
        $this->basePrice = (float)$basePrice;
        $this->commission = (float)$commission;
        $this->policy = (float)$policy;
        $this->tax = (float)$tax;
        $this->_generate($installment);
    }

    public function getInstallments()
    {
        return $this->_installments;
    }

    private function _generate($installment)
    {
        $taxEqually = $this->_divideValue($this->tax, $installment);
        $policyEqually = $this->_divideValue($this->policy, $installment);
        $commissionEqually = $this->_divideValue($this->commission, $installment);

        for($i = 0; $i < $installment; $i++)
        {
            $this->_installments[] = new Installment(
                $this->basePrice, $policyEqually[$i], $commissionEqually[$i], $taxEqually[$i]);
        }
    }

    private function _divideValue($num, $installment)
    {
        $val = floor($num / $installment);

        for ($i = 0; $i < $installment; $i++)
        {
            $arr[$i] = $val;
        }

        $arr[0] += $num - array_sum($arr);

        return $arr;
    }
}
