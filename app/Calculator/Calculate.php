<?php

namespace App\Calculator;

use App\Calculator\Interfaces\CalculateInterface;
use App\Calculator\Model\BasePolicy;
use App\Calculator\Model\Tax;
use App\Calculator\Model\Commission;

class Calculate implements CalculateInterface
{

    private $_basePrice;
    private $_taxRate;

    private $_basePolicy;
    private $_commission;
    private $_tax;

    public function __construct($basePrice, $taxRate)
    {
        $this->_basePrice = $basePrice;
        $this->_taxRate = $taxRate;
    }

    public function calculateBasePolicy()
    {
        $this->_basePolicy = new CalculateBasePolicy(new BasePolicy($this->_basePrice));
        $this->_basePolicy->calculateBasePolicy();
    }

    public function calculateCommission()
    {
        $this->_commission = new CalculateCommission(new Commission($this->_basePrice));
        $this->_commission->calculateCommission();
    }

    public function calculateTax()
    {
        $this->_tax =  new CalculateTax(new Tax($this->_taxRate, $this->_basePrice));
        $this->_tax->calculateTax();
    }

    public function getTax()
    {
        return $this->_tax->getTax();
    }

    public function getCommission()
    {
        return $this->_commission->getCommissionModel();
    }

    public function getBasePolicy()
    {
        return $this->_basePolicy->getBasePolicy();
    }

    public function getBasePrice()
    {
        return $this->_basePrice;
    }

}
