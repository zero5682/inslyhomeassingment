<?php

namespace App\Calculator;

use App\Calculator\Model\BasePolicy;

class CalculateBasePolicy
{

    private $_baseRate = 11;
    private $_specialRate = 13;

    private $_basePolicy;

    private $_specialTimeStart;
    private $_specialTimeTo;

    public function __construct(BasePolicy $basePolicy)
    {
        $this->_basePolicy = $basePolicy;
        $this->_specialTimeStart = strtotime("15:00:00");
        $this->_specialTimeTo = strtotime("20:00:00");
    }

    public function calculateBasePolicy()
    {
        $currentDate = date('h:i:s', time());

        $this->_currentRate = $currentDate >= $this->_specialTimeStart && $currentDate <= $this->_specialTimeTo
            ? $this->_specialRate : $this->_baseRate;

        $this->_basePolicy->setCurrentRate($this->_currentRate);
        $this->_basePolicy->setBasePolicyPrice(
            ($this->_currentRate / 100) * $this->_basePolicy->getBasePrice()
        );

        return $this;
    }

    public function getBasePolicy()
    {
        return $this->_basePolicy;
    }

    public function getCurrentRate()
    {
        return $this->_currentRate;
    }

}
