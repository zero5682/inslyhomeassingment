<?php

namespace App\Calculator;

use App\Calculator\Interfaces\CommissionInterface;
use App\Calculator\Model\Commission as CommissionModel;

class CalculateCommission implements CommissionInterface
{
    private $_commissionRate = 17;

    private $_commissionModel;

    public function __construct(CommissionModel $commissionModel)
    {
        $this->_commissionModel = $commissionModel;
        $this->_commissionModel->setCommissionRate($this->_commissionRate);
    }

    public function calculateCommission()
    {
        $this->_commissionModel->setCalculatedPrice(
            ($this->_commissionRate / 100) * $this->_commissionModel->getBasePrice()
        );
        return $this;
    }

    public function getCommissionModel()
    {
        return $this->_commissionModel;
    }




}
