<?php

namespace App\Calculator\Interfaces;

interface BasePolicyInterface
{
    public function calculateBasePolicy();
}
