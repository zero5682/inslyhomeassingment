<?php

namespace App\Calculator\Interfaces;

interface CalculateInterface
{
    public function calculateBasePolicy();
    public function calculateCommission();
    public function calculateTax();
}
