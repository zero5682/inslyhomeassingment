<?php

namespace App\Calculator\Interfaces;

interface TaxInterface
{
    public function calculateTax();
}
