<?php

namespace App\Calculator\Interfaces;

interface CommissionInterface
{
    public function calculateCommission();
}
