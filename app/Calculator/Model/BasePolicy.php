<?php

namespace App\Calculator\Model;

class BasePolicy
{
    private $_basePolicyPrice;
    private $_basePrice;
    private $_currentRate;

    public function __construct($basePrice)
    {
        $this->_basePrice = $basePrice;
    }

    public function getBasePolicyPrice()
    {
        return $this->_basePolicyPrice;
    }

    public function setBasePolicyPrice($basePolicyPrice)
    {
        $this->_basePolicyPrice = $basePolicyPrice;
        return $this;
    }

    public function getCurrentRate()
    {
        return $this->_currentRate;
    }

    public function setCurrentRate($currentRate)
    {
        $this->_currentRate = $currentRate;
        return $this;
    }

    public function getBasePrice()
    {
        return $this->_basePrice;
    }
}
