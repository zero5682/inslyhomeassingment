<?php

namespace App\Calculator\Model;

class Commission {

    private $_basePrice;
    private $_commissionRate;
    private $_calculatedPrice;

    public function __construct($basePrice)
    {
        $this->_basePrice = $basePrice;
    }

    public function setCalculatedPrice($calculatedPrice)
    {
        $this->_calculatedPrice = $calculatedPrice;
        return $this;
    }

    public function getCalculatedPrice()
    {
        return $this->_calculatedPrice;
    }

    public function getBasePrice()
    {
        return $this->_basePrice;
    }

    public function setCommissionRate($commissionRate)
    {
        $this->_commissionRate = $commissionRate;
        return $this;
    }

    public function getCommissionRate()
    {
        return $this->_commissionRate;
    }

}
