<?php

namespace App\Calculator\Model;

class Tax
{

    private $_tax;

    private $_basePrice;

    private $_calculatedPrice;

    public function __construct($tax, $basePrice)
    {
        $this->_tax = $tax;
        $this->_basePrice = $basePrice;
    }

    public function getBasePrice()
    {
        return $this->_basePrice;
    }

    public function getTax()
    {
        return $this->_tax;
    }

    public function setCalculatedTaxPrice($price)
    {
        $this->_calculatedPrice = $price;
        return $this;
    }

    public function getCalculatedTaxPrice()
    {
        return $this->_calculatedPrice;
    }
}
