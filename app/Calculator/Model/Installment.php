<?php

namespace App\Calculator\Model;

class Installment
{
    public $basePrice;
    public $commission;
    public $tax;
    public $policy;
    public $grand_total;

    public function __construct($basePrice, $policy, $commission, $tax)
    {
        $this->basePrice = $basePrice;
        $this->commission = $commission;
        $this->policy = $policy;
        $this->tax = $tax;
        $this->grand_total = ($this->policy + $this->commission + $this->tax);
    }
}
