<?php

namespace App\Calculator\Model;

class Calculate
{

    private $_tax;
    private $_commission;
    private $_basePolicy;

    public function __construct(Commission $commission, Tax $tax, BasePolicy $basePolicy)
    {
        $this->_basePolicy = $basePolicy;
        $this->_commission = $commission;
        $this->_tax = $tax;
    }

    public function getBasePolicy()
    {
        return $this->_basePolicy;
    }

    public function getCommission()
    {
        return $this->_commission;
    }

    public function getTax()
    {
        return $this->_tax;
    }
}
