<?php

namespace App\Calculator;

use App\Calculator\Interfaces\TaxInterface;
use App\Calculator\Model\Tax;

class CalculateTax implements TaxInterface
{
    private $_tax;

    public function __construct(Tax $tax)
    {
        $this->_tax = $tax;
    }

    public function calculateTax()
    {
        $this->_tax->setCalculatedTaxPrice(
            ($this->_tax->getTax() / 100) * $this->_tax->getBasePrice()
        );
        return $this;
    }

    public function getTax()
    {
        return $this->_tax;
    }
}
