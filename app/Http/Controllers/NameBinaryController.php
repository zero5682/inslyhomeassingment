<?php

namespace App\Http\Controllers;

class NameBinaryController extends Controller
{

    public function index()
    {
        $bin = "01110000 01110010 01101001 01101110 01110100 00100000 01101111 01110101
                01110100 00100000 01111001 01101111 01110101 01110010 00100000 01101110
                01100001 01101101 01100101 00100000 01110111 01101001 01110100 01101000
                00100000 01101111 01101110 01100101 00100000 01101111 01100110 00100000
                01110000 01101000 01110000 00100000 01101100 01101111 01101111 01110000
                01110011";
        $binaries = explode(' ', $bin);

        $question = null;
        foreach ($binaries as $binary) {
            $question .= pack('H*', dechex(bindec($binary)));
        }

        echo $question . "<br/><br/>";

        $name = "Karl";

        $nameSplit = str_split($name);

        foreach($nameSplit as $n) {
            echo  $n;
        }

        return view("binary.name");
    }
}
