<?php

namespace App\Http\Controllers;

use App\Calculator\Calculate;
use App\Calculator\Installments;
use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function index()
    {
        return view("calculator.index");
    }

    public function calculate(Request $request)
    {
        $calculator = new Calculate($request->esimatedCarValue, $request->taxPercentage);

        $calculator->calculateBasePolicy();
        $calculator->calculateCommission();
        $calculator->calculateTax();

        $installments = null;

        if ($request->numberInstalments > 0)
        {
            $installments = new Installments(
                $calculator->getBasePrice(),
                $calculator->getBasePolicy()->getBasePolicyPrice(),
                $calculator->getCommission()->getCalculatedPrice(),
                $calculator->getTax()->getCalculatedTaxPrice(),
                $request->numberInstalments
            );
        }

        return response()->json([
            "base_price" => (int)$calculator->getBasePrice(),
            "commission" => $calculator->getCommission()->getCalculatedPrice(),
            "tax" => $calculator->getTax()->getCalculatedTaxPrice(),
            "installments" => $installments->getInstallments(),
            "commission_rate" => $calculator->getCommission()->getCommissionRate(),
            "base_policy" => $calculator->getBasePolicy()->getBasePolicyPrice(),
            "base_policy_rate" => $calculator->getBasePolicy()->getCurrentRate(),
            "tax_rate" => $calculator->getTax()->getTax(),
            "grand_total" => $calculator->getCommission()->getCalculatedPrice() + $calculator->getTax()->getCalculatedTaxPrice() + $calculator->getBasePolicy()->getBasePolicyPrice()
        ]);
    }
}
