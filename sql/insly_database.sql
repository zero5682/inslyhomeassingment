-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.6027
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for insly_employee
CREATE DATABASE IF NOT EXISTS `insly_employee` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `insly_employee`;

-- Dumping structure for table insly_employee.contact
CREATE TABLE IF NOT EXISTS `contact` (
  `contact_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`contact_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly_employee.contact: 1 rows
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` (`contact_id`, `email`, `phone`, `address`, `user_id`) VALUES
	(1, 'test@testing.com', '12345666', 'test aadress', 1);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

-- Dumping structure for table insly_employee.language
CREATE TABLE IF NOT EXISTS `language` (
  `language_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly_employee.language: 3 rows
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` (`language_id`, `name`, `code`) VALUES
	(1, 'english', 'en'),
	(2, 'spanish', 'es'),
	(3, 'french', 'fr');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;

-- Dumping structure for table insly_employee.localization
CREATE TABLE IF NOT EXISTS `localization` (
  `localization_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `introduction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `previous_work_experience` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `education_information` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`localization_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly_employee.localization: 3 rows
/*!40000 ALTER TABLE `localization` DISABLE KEYS */;
INSERT INTO `localization` (`localization_id`, `introduction`, `previous_work_experience`, `education_information`, `language_id`, `user_id`) VALUES
	(1, 'test introduction en', 'test work experience', 'test information', '1', 1),
	(2, 'test introduction es', 'test work experience', 'test information', '2', 1),
	(3, 'test introduction fr', 'test work experience', 'test information', '3', 1);
/*!40000 ALTER TABLE `localization` ENABLE KEYS */;

-- Dumping structure for table insly_employee.log
CREATE TABLE IF NOT EXISTS `log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT 0,
  `modified_date` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly_employee.log: 2 rows
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` (`log_id`, `user_id`, `modified_date`, `created_date`) VALUES
	(1, 1, '2020-07-14 04:29:25', '2020-07-14 04:29:26'),
	(2, 1, '2020-07-14 04:29:30', '2020-07-14 04:29:30');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;

-- Dumping structure for table insly_employee.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssn_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_current_employee` tinyint(3) unsigned NOT NULL,
  `contact_id` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table insly_employee.user: 1 rows
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `name`, `ssn_id`, `is_current_employee`, `contact_id`) VALUES
	(1, 'test', '12435', 1, 1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
